

set_subclass = {
	switch = {
		trigger = has_modifier
		boundless_ocean = { 
			set_planet_entity = {
				picture = pc_nf_deep
				entity = deep_planet_01_entity
			} 
		}
		swamp_world = { 
			set_planet_entity = {
				picture = pc_nf_swamp
				entity = swamp_planet_01_entity
			}
		}
		glacial_world = { 
			set_planet_entity = {
				picture = pc_nf_glacial
				entity = glacial_planet_01_entity
			}
		}
		young_world = { 
			set_planet_entity = {
				picture = pc_nf_young
				entity = young_planet_01_entity
			}
		}
		tempest_world = { 
			set_planet_entity = {
				picture = pc_nf_tempest
				entity = tempest_planet_01_entity
			}
		}
		dune_world = { 
			set_planet_entity = {
				picture = pc_nf_dune
				entity = dune_planet_01_entity
			}
		}
		karstic_world = { 
			set_planet_entity = {
				picture = pc_nf_karstic
				entity = karstic_planet_01_entity
			}
		}
		boreal_world = { 
			set_planet_entity = {
				picture = pc_nf_boreal
				entity = boreal_planet_01_entity
			}
		}
		forest_world = { 
			set_planet_entity = {
				picture = pc_nf_forest
				entity = forest_planet_01_entity
			}
		}
		fungal_world = { 
			set_planet_entity = {
				picture = pc_nf_fungal
				entity = fungal_planet_01_entity
			}
		}
		poisonous_world = { 
			set_planet_entity = {
				picture = pc_nf_poisonous
				entity = poisonous_planet_01_entity
			}
		}
		geothermal_world = { 
			set_planet_entity = {
				picture = pc_nf_geothermal
				entity = geothermal_planet_01_entity
			}
		}
		underground_world = { 
			set_planet_entity = {
				picture = pc_nf_underground
			}
		}
		
		sulfur_world = { 
			set_planet_entity = {
				picture = pc_nf_sulfur
				entity = sulfur_planet_01_entity
			}
		}
		greenhouse_world = { 
			set_planet_entity = {
				picture = pc_nf_greenhouse
				entity = greenhouse_planet_01_entity
			}
			random_list = {
				35 = {
					set_planet_entity = {
						picture = pc_nf_greenhouse
						entity = greenhouse_planet_01_entity
					}
				}
				35 = {
					set_planet_entity = {
						picture = pc_nf_greenhouse
						entity = greenhouse_planet_02_entity
					}
				}
				30 = {
					set_planet_entity = {
						picture = pc_nf_greenhouse
						entity = greenhouse_planet_03_entity
					}
				}
			}
		}
		methane_world = { 
			set_planet_entity = {
				picture = pc_nf_methane
				entity = methane_planet_01_entity
			}
		}
		planetoid = { 
			set_planet_entity = {
				picture = pc_nf_planetoid
				entity = planetoid_planet_01_entity
			}
		}
		ice_planetoid = { 
			set_planet_entity = {
				picture = pc_nf_ice_planetoid
				entity = ice_planetoid_planet_01_entity
			}
		}
		frozen_ocean = { 
			set_planet_entity = {
				picture = pc_nf_frozen_ocean
				entity = frozen_ocean_planet_01_entity
			}
		}
		hot_jupiter = { 
			set_planet_entity = {
				picture = pc_nf_hot_giant
			}
		}
		puffy_planet = { 
			set_planet_entity = {
				picture = pc_nf_gas_supergiant
			}
		}
		ice_giant = { 
			set_planet_entity = {
				picture = pc_nf_ice_giant
			}
		}
		scorched_world = { 
			set_planet_entity = {
				picture = pc_nf_scorched
				entity = scorched_planet_01_entity
			}
		}
		atoll_world = { 
			set_planet_entity = {
				picture = pc_nf_atoll
				entity = atoll_planet_01_entity
			}
		}
		ice_world = { 
			set_planet_entity = {
				picture = pc_nf_ice
			}
		}
		dust_world = { 
			set_planet_entity = {
				picture = pc_nf_dust
				entity = dust_planet_01_entity
			}
		}
		oasis_world = { 
			set_planet_entity = {
				picture = pc_nf_oasis
				entity = oasis_planet_01_entity
			}
		}
		salt_world = { 
			set_planet_entity = {
				picture = pc_nf_salt
				entity = salt_planet_01_entity
			}
		}
		steppe_world = { 
			set_planet_entity = {
				picture = pc_nf_steppe
				entity = steppe_planet_01_entity
			}
		}
		snow_world = { 
			set_planet_entity = {
				picture = pc_nf_snow
				entity = snow_planet_01_entity
			}
		}
		antarctic_world = { 
			set_planet_entity = {
				picture = pc_nf_antarctic
				entity = antarctic_planet_01_entity
			}
		}
		temperate_zone = { 
			set_planet_entity = {
				picture = pc_nf_temperate
				entity = temperate_planet_01_entity
			}
		}
		cold_desert = { 
			set_planet_entity = {
				picture = pc_nf_cold_desert
				entity = cold_desert_01_entity
			}
		}
		pangea = { 
			set_planet_entity = {
				picture = pc_nf_pangea
				entity = pangea_planet_01_entity
			}
		}
		algic_seas = { 
			set_planet_entity = {
				picture = pc_nf_algic
				entity = algic_planet_01_entity
			}
		}
		rocky_world = { 
			set_planet_entity = {
				picture = pc_nf_rocky
				entity = rocky_planet_01_entity
			}
		}
		monsoon_world = { 
			set_planet_entity = {
				picture = pc_nf_monsoon
				entity = monsoon_planet_01_entity
			}
		}
		ancient_world = { 
			set_planet_entity = {
				picture = pc_nf_ancient
				entity = ancient_planet_01_entity
			}
		}
		industrial_graveyard = { 
			set_planet_entity = {
				picture = pc_nf_graveyard
				entity = graveyard_planet_01_entity
			}
		}
		volcanic_world = {
			random_list = {
				50 = {
					set_planet_entity = {
						picture = pc_nf_volcanic
						entity = volcanic_planet_01_entity
					}
				}
				50 = {
					set_planet_entity = {
						picture = pc_nf_volcanic
						entity = volcanic_planet_02_entity
					}
				}
			}
		}
		carbon_world = { 
			set_planet_entity = {
				picture = pc_nf_carbon
				entity = carbon_planet_01_entity
			}
		}
		hot_barren_world = { 
			set_planet_entity = {
				picture = pc_nf_hot_barren
				entity = hot_barren_planet_01_entity
			}
		}
		ammonia_world = { 
			set_planet_entity = {
				picture = pc_nf_ammonia
				entity = ammonia_planet_01_entity
			}
		}
		primal_world = { 
			set_planet_entity = {
				picture = pc_nf_primal
				entity = primal_planet_01_entity
			}
		}
		mesa_world = { 
			set_planet_entity = {
				picture = pc_nf_mesa
				entity = mesa_planet_01_entity
			}
		}
	}
}

remove_subclass_modifiers = {
	if = {
		limit = {  has_modifier = "dune_world" }
		remove_modifier = "dune_world"
	}
	if = {
		limit = {  has_modifier = "underground_world" }
		remove_modifier = "underground_world"
	}
	if = {
		limit = {  has_modifier = "karstic_world" }
		remove_modifier = "karstic_world"
	}
	if = {
		limit = {  has_modifier = "young_world" }
		remove_modifier = "young_world"
	}
	if = {
		limit = {  has_modifier = "boundless_ocean" }
		remove_modifier = "boundless_ocean"
	}
	if = {
		limit = {  has_modifier = "swamp_world" }
		remove_modifier = "swamp_world"
	}
	if = {
		limit = {  has_modifier = "poisonous_world" }
		remove_modifier = "poisonous_world"
	}
	if = {
		limit = {  has_modifier = "fungal_world" }
		remove_modifier = "fungal_world"
	}
	if = {
		limit = {  has_modifier = "boreal_world" }
		remove_modifier = "boreal_world"
	}
	if = {
		limit = {  has_modifier = "forest_world" }
		remove_modifier = "forest_world"
	}
	if = {
		limit = {  has_modifier = "tempest_world" }
		remove_modifier = "tempest_world"
	}
	if = {
		limit = {  has_modifier = "geothermal_world" }
		remove_modifier = "geothermal_world"
	}
	if = {
		limit = {  has_modifier = "glacial_world" }
		remove_modifier = "glacial_world"
	}
	if = {
		limit = {  has_modifier = "sulfur_world" }
		remove_modifier = "sulfur_world"
	}
	if = {
		limit = {  has_modifier = "greenhouse_world" }
		remove_modifier = "greenhouse_world"
	}
	if = {
		limit = {  has_modifier = "methane_world" }
		remove_modifier = "methane_world"
	}
	if = {
		limit = {  has_modifier = "planetoid" }
		remove_modifier = "planetoid"
	}
	if = {
		limit = {  has_modifier = "ice_planetoid" }
		remove_modifier = "ice_planetoid"
	}
	if = {
		limit = {  has_modifier = "frozen_ocean" }
		remove_modifier = "frozen_ocean"
	}
	if = {
		limit = {  has_modifier = "hot_jupiter" }
		remove_modifier = "hot_jupiter"
	}
	if = {
		limit = {  has_modifier = "puffy_planet" }
		remove_modifier = "puffy_planet"
	}
	if = {
		limit = {  has_modifier = "ice_giant" }
		remove_modifier = "ice_giant"
	}
	if = {
		limit = {  has_modifier = "scorched_world" }
		remove_modifier = "scorched_world"
	}
	if = {
		limit = {  has_modifier = "oasis_world" }
		remove_modifier = "oasis_world"
	}
	if = {
		limit = {  has_modifier = "salt_world" }
		remove_modifier = "salt_world"
	}
	if = {
		limit = {  has_modifier = "dust_world" }
		remove_modifier = "dust_world"
	}
	if = {
		limit = {  has_modifier = "steppe_world" }
		remove_modifier = "steppe_world"
	}
	if = {
		limit = {  has_modifier = "atoll_world" }
		remove_modifier = "atoll_world"
	}
	if = {
		limit = {  has_modifier = "algic_seas" }
		remove_modifier = "algic_seas"
	}
	if = {
		limit = {  has_modifier = "rocky_world" }
		remove_modifier = "rocky_world"
	}
	if = {
		limit = {  has_modifier = "monsoon_world" }
		remove_modifier = "monsoon_world"
	}
	if = {
		limit = {  has_modifier = "pangea" }
		remove_modifier = "pangea"
	}
	if = {
		limit = {  has_modifier = "snow_world" }
		remove_modifier = "snow_world"
	}
	if = {
		limit = {  has_modifier = "cold_desert" }
		remove_modifier = "cold_desert"
	}
	if = {
		limit = {  has_modifier = "ice_world" }
		remove_modifier = "ice_world"
	}
	if = {
		limit = {  has_modifier = "antarctic_world" }
		remove_modifier = "antarctic_world"
	}
	if = {
		limit = {  has_modifier = "temperate_zone" }
		remove_modifier = "temperate_zone"
	}
	if = {
		limit = {  has_modifier = "ancient_world" }
		remove_modifier = "ancient_world"
	}
	if = {
		limit = {  has_modifier = "industrial_graveyard" }
		remove_modifier = "industrial_graveyard"
	}
	if = {
		limit = {  has_modifier = "volcanic_world" }
		remove_modifier = "volcanic_world"
	}
	if = {
		limit = {  has_modifier = "carbon_world" }
		remove_modifier = "carbon_world"
	}
	if = {
		limit = {  has_modifier = "ammonia_world" }
		remove_modifier = "ammonia_world"
	}
	if = {
		limit = {  has_modifier = "hot_barren_world" }
		remove_modifier = "hot_barren_world"
	}
	if = {
		limit = {  has_modifier = "primal_world" }
		remove_modifier = "primal_world"
	}
	if = {
		limit = {  has_modifier = "mesa_world" }
		remove_modifier = "mesa_world"
	}
}





