[h1][b]RS v2.3.3 (Current Version)[/b][/h1]

Updated to Wolfe v2.3.3.

See [b]Change Notes[/b] for more details: [url=https://steamcommunity.com/sharedfiles/filedetails/changelog/725596168/]link[/url]

[b][u]IMPORTANT NOTE REGARDING LEGACY SUPPORT[/u][/b]

For those using beta/older versions of Stellaris, [b][u]do not[/u][/b] use this mod. Instead, use this legacy version of RS (Boulle v1.9.1): [url=https://steamcommunity.com/sharedfiles/filedetails/?id=1313232062]link[/url]


[h1][b]MASTER COMPATIBILITY FIX LIST[/b][/h1]

It is [b]highly recommended[/b] to check this list if you use other mods: [url=https://steamcommunity.com/workshop/filedetails/discussion/725596168/1326718197224225237/]link[/url]

Please understand that I made this mod using my own free time but I will never, ever, ask for compensation or donation of any kind. I do this for fun and I wanted to share the things I enjoy with like-minded people. This mod is not for everybody (read WHO IS THIS MOD FOR? section below) and I am pretty adamant about sticking to my mod design goals so I ask for your understanding if a particular feature or compatibility fix request is not always granted.


[h1][b]LOCALISATION[/b][/h1]
[list]
[*]Russian [i](by Spectrum)[/i][b]:[/b] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=1375388095]link[/url]
[*]German [i](by p6kocka)[/i][b]:[/b] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=943660723]link[/url]
[*]Korean [i](by parkon9)[/i][b]:[/b] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=1206501804]link[/url]
[*]French [i](by pipo.p)[/i][b]:[/b] built-in
[/list]


[h1][b]ABOUT THIS MOD[/b][/h1]

There are a lot of mods out there that add new ship classes. Some I find quite good, others I find excessive (too many turrets, too much shield/armor/power). My goal was to do the same while maintaining a more realistic aesthetic and gameplay that is hopefully in line with current section/ship game design goals. 

This mod adds 5 new ship classes: 
[list]
[*]Support Cruiser [url=https://steamcommunity.com/sharedfiles/filedetails/changelog/725596168?p=5]link[/url]
[*]Electronic Attack Cruiser [url=https://steamcommunity.com/sharedfiles/filedetails/changelog/725596168?p=5]link[/url]
[*]Battlecruiser [url=https://steamcommunity.com/sharedfiles/filedetails/changelog/725596168?p=6]link[/url]
[*]Dreadnought [url=https://steamcommunity.com/sharedfiles/filedetails/changelog/725596168?p=6]link[/url] 
[*]Heavy Dreadnought [url=https://steamcommunity.com/sharedfiles/filedetails/changelog/725596168?p=6]link[/url]
[/list]
No pre-requisite tech is required to unlock the new ship classes except for support cruisers and e-attack cruisers (refer to the [b]Change Notes[/b] tab for details). Support cruisers, e-attack cruisers, and battlecruisers should become available at the same time as cruisers while dreadnoughts and heavy dreadnoughts at the same time as battleships. XL sections will unlock once you have researched at least one XL weapon. Take note that some heavy dreadnoughts will only have one bow section (spinal mount) available (molluscoid, etc) so you won't be able to complete the design unless you have researched at least one XL weapon.

Battlecruisers are treated as a medium/heavy variant within the cruiser class, with vanilla cruisers considered as the light variant. Battlecruisers are therefore the heaviest sub-capital ship class available. For the capital class ships, battleships are considered the light variant, dreadnoughts medium, and heavy dreadnoughts the heavy variants. Following this reasoning, there is no need for enabling techs, just increasing build cost/time the heavier the variant is.

Should bigger classes be introduced in the future, those will fall under the super-capital class and at that point, it makes sense to require a new shipyard level and/or enabling tech(s).

Click the [b]Change Notes[/b] tab for details on each ship class. 


[h1][b]WHO IS THIS MOD FOR?[/b][/h1]

This mod is for those who feel the current vanilla ship roster needs more diversity without making sweeping changes to the vanilla game. This is a very simple mod focused mainly on adding new ship classes. The new ship classes in this mod are very complimentary and probably as balanced as they come (against vanilla ships), since the new classes merely adds additional ship section(s) with minimal to no changes to the existing vanilla ship section designs.

For those who are looking for more than just adding ship types, there are a lot of complimentary mods out there that add new tech/weapons, features, and gameplay. It may not be my personal preference but who says you can't try those mods with mine?. :)


[h1][b]SPECIAL THANKS[/b][/h1]

I want to thank the special contributions these good people have made:
[list]
[*][b]CrusaderVanguard[/b] for the big little reminders every now and then!
[*][b]folk[/b] for the awesome coding suggestions!
[*][b]Inquisitor Kot[/b] for play-testing and sharing a host of other stuff *wink*
[/list]
Check out the cool mods of these guys in the link above.


[h1][b]OTHER STUFF[/b][/h1]

All screenshots were taken with the excellent UI Overhaul 1440p mod enabled:
[list]
[*][b]UI Overhaul 1440p:[/b] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=684450485]link[/url]
[/list]
Feel free to use the files and/or concepts used in this mod for any of your projects. If you do, a little note in your mod for me would be appreciated. :)

If you like my mod, please don't forget to leave a thumbs up. If you don't like the mod, instead of simply leaving a thumbs down, let me know why so I can do something about it, if I can.


[h1][b]WORK IN PROGRESS:[/b][/h1]

[b]Prototype Titan Super Capital Class Voidcraft[/b]

Mammalian - https://steamcommunity.com/sharedfiles/filedetails/?id=951839222
Arthropoid - https://steamcommunity.com/sharedfiles/filedetails/?id=959433197
Molluscoid - https://steamcommunity.com/sharedfiles/filedetails/?id=1229645148

[b]UPDATE:[/b] Unfortunately, Apocalypse was released and other things have been taking up my time so I am unable to continue with the titan project. I might revisit this project later on and introduce these as flagships.

Thank you for looking.